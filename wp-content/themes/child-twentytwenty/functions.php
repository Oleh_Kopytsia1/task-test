<?php
class ChildThemeTwentytwenty{

    private $quantity_discount = 3;

    /**
     * ChildThemeTwentytwenty constructor.
     * Creation of actions and filters.
     */
    public function __construct()
    {
        add_action( 'wp_enqueue_scripts', [$this, 'theme_enqueue_styles'] );
        add_filter( 'woocommerce_cart_item_subtotal', [$this, 'filter_woocommerce_cart_item_subtotal'], 10, 3 );
        add_action( 'woocommerce_cart_calculate_fees', [$this, 'add_custom_discount'], 10, 1 );
        add_action( 'woocommerce_new_order', [$this, 'create_invoice_for_wc_order'],  10, 1  );
        add_filter( 'woocommerce_email_order_items_table', [$this, 'email_order_items_table'], 10, 2 );
    }

    /**
     * Connecting styles from the parent theme.
     */
    public function theme_enqueue_styles()
    {
        $parent_style = 'parent-style';
        wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'child-style',
            get_stylesheet_directory_uri() . '/style.css',
            [ $parent_style ],
            wp_get_theme()->get('Version')
        );
    }

    /**
     * Get array free products for processing.
     * @param $cart
     * @return mixed
     */
    private function get_free_products($cart)
    {
        $cart_products_quantity = 0;

        foreach ( $cart->get_cart() as $item ) {
            $cart_products[] = [
                'product_id' => $item['product_id'],
                'price' => $item['data']->get_price(),
                'quantity' => $item['quantity']
            ];

            $cart_products_quantity += $item['quantity'];
        }

        array_multisort(array_map(function($item) {
            return $item['price'];
        }, $cart_products), SORT_ASC, $cart_products);

        $quantity_free_products = (int)($cart_products_quantity / $this->quantity_discount);

        $first_pass = true;
        $count = 0;
        foreach ($cart_products as $item ) {
            $count += $item['quantity'];

            if($first_pass && $count >= $quantity_free_products) {
                $free_products[$item['product_id']] = $quantity_free_products;
                break 1;
            }else if($count < $quantity_free_products) {
                $free_products[$item['product_id']] = $item['quantity'];
            }else if($count >= $quantity_free_products){
                $free_products[$item['product_id']] = ($quantity_free_products - ($count - $item['quantity']));
                break 1;
            }

            $first_pass = false;
        }

        return $free_products;
    }

    /**
     * Filter for the subtotal on the cart and checkout.
     * @param $subtotal
     * @param $cart_item
     * @param $cart_item_key
     * @return string
     */
    public function filter_woocommerce_cart_item_subtotal( $subtotal, $cart_item, $cart_item_key )
    {
        global $woocommerce;
        $free_products = $this->get_free_products($woocommerce->cart);

        if(isset($free_products[$cart_item['product_id']])){
            $discount = $free_products[$cart_item['product_id']] * $cart_item['data']->get_price();
            $price = $cart_item['line_subtotal'] - $discount;
            $subtotal = wc_price($price)."<div>Discount</div><div>".wc_price(-$discount)."</div>";
        }

	    return $subtotal;
    }

    /**
     * Adding a discount.
     * @param $wc_cart
     */
    public function add_custom_discount( $wc_cart )
    {
        global $woocommerce;
        $free_products = $this->get_free_products($woocommerce->cart);

        $discount = 0;
        foreach ( $woocommerce->cart->get_cart() as $item ) {
            if(isset($free_products[$item['product_id']])){
                $price = $free_products[$item['product_id']] * $item['data']->get_price();
                $discount += $price;
            }
        }

        $wc_cart->add_fee( 'Discount', -$discount, true  );
    }

    /**
     * Save array free products for processing.
     * @param $order_id
     */
    public function create_invoice_for_wc_order( $order_id )
    {
        global $woocommerce;
        $free_products = $this->get_free_products($woocommerce->cart);
        update_post_meta($order_id, '_cart_item_subtotal_discount', $free_products);
    }

    /**
     * Adding a discount to a mail template.
     * @param $table_tbody
     * @param $order
     * @return string
     */
    public function email_order_items_table($table_tbody, $order){
        $free_products = get_post_meta($order->get_id(), '_cart_item_subtotal_discount', true);

        $order = wc_get_order($order->id);
        $wpf = new WC_Product_Factory();
        $html_tbody = '';
        foreach ($order->get_items() as $item_key => $item ){
            $product_id   = $item->get_product_id();
            $product = $wpf->get_product($product_id);
            $name = $item->get_name();
            $quantity = $item->get_quantity();
            $discount = 0;
            if(isset($free_products[$product_id])){
                $discount += $free_products[$product_id] * $product->get_price();
                $subtotal_discount = wc_price(($product->get_price() * $item->get_quantity()) - $discount)."<div>Discount</div><div>-".wc_price($discount).'</div>';
            }else{
                $subtotal_discount = wc_price($item->get_subtotal());
            }

            $html_tbody .= <<<HTML
<tr class="order_item">
    <td class="td" style="text-align:left; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
        $name
    </td>
    <td class="td" style="text-align:left; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
        $quantity
    </td>
    <td class="td" style="text-align:left; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
        $subtotal_discount
    </td>
</tr>
HTML;
        }

        return $html_tbody;
    }

}

new ChildThemeTwentytwenty();
